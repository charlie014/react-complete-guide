import "./Card.css"

function Card(props) {
    const classes = 'card ' + props.className; /* all the classname from outside will be attached to this card class */
    return (
        <div className={classes}>
            {/*children is a reserve name for the whole content inside the Card component*/}
            {props.children}
        </div>
    )
}

export default Card;

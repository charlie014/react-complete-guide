import ExpenseItem from "../ExpenseItem/ExpenseItem";
import "./ExpensesList.css"

const ExpensesList = (props) => {
    let expensesContent = <p style={{color: "white"}}>No Expenses Found.</p>

    if (props.items.length === 0) {
        return (<h2 className="expense-list__fallback">
            Found No Expenses.
        </h2>)
    }
    if (props.items.length > 0) {
        expensesContent = props.items.map((expense) => (
            <ExpenseItem
                key={expense.id} //any primitive types can be used as an unique ID
                title={expense.title}
                amount={expense.amount}
                date={expense.date}
            />
        ))
    }

    return (
        <ul className="expenses-list">
            {expensesContent}
        </ul>
    )
}

export default ExpensesList

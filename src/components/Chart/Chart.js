import "./Chart.css"
import ChartBar from "./ChartBar"

const Chart = (props) => {
    const dataPointValues = props.dataPoints.map(dataPoint => dataPoint.value);  //map and return new array that contains value only
    const totalMaximum = Math.max(...dataPointValues) //using spread to take the value only out of the array
    return (
        <div className="chart">
            {props.dataPoints.map(dataPoint =>
                <ChartBar
                    key={dataPoint.label}
                    value = {dataPoint.value}
                    maxValue={totalMaximum}
                    label={dataPoint.label}
                />
            )}
        </div>
    )
}

export default Chart
